# -*- coding: utf-8 *-*
import utils
from collections import OrderedDict

PLAN_DE_CUENTAS = OrderedDict()
# CODIGO              NOMBRE                  INPUTABLE
PLAN_DE_CUENTAS["1.0.00.00.000"] = ("Activo", False)
PLAN_DE_CUENTAS["1.1.00.00.000"] = ("Activo Corriente", False)
PLAN_DE_CUENTAS["1.1.01.00.000"] = ("Disponibilidades", False)
PLAN_DE_CUENTAS["1.1.01.00.001"] = ("Caja", True)
PLAN_DE_CUENTAS["1.1.01.00.002"] = ("Bancos", True)
PLAN_DE_CUENTAS["1.1.02.00.000"] = ("Créditos Sociales", False)
PLAN_DE_CUENTAS["1.1.02.01.000"] = ("Cuotas a Cobrar", False)
PLAN_DE_CUENTAS["1.2.00.00.000"] = ("Activo No Corriente", False)
PLAN_DE_CUENTAS["1.2.01.00.000"] = ("Bienes de Uso", False)
PLAN_DE_CUENTAS["1.2.02.00.000"] = ("Créditos", False)
PLAN_DE_CUENTAS["1.2.02.00.001"] = ("Plazo Fijo", False)
PLAN_DE_CUENTAS["2.0.00.00.000"] = ("Pasivo", False)
PLAN_DE_CUENTAS["3.0.00.00.000"] = ("Patrimonio Neto", False)
PLAN_DE_CUENTAS["4.0.00.00.000"] = ("Ingresos", False)
PLAN_DE_CUENTAS["4.0.00.01.000"] = ("Cuotas Sociales", False)
PLAN_DE_CUENTAS["4.0.00.01.001"] = ("Aportes Mensuaes", True)
PLAN_DE_CUENTAS["4.0.00.02.000"] = ("Donaciones de Terceros", False)
PLAN_DE_CUENTAS["5.0.00.00.000"] = ("Egresos", False)
PLAN_DE_CUENTAS["5.1.00.00.000"] = ("Gastos Corrientes", False)
PLAN_DE_CUENTAS["5.1.01.00.000"] = ("Servicios", False)
PLAN_DE_CUENTAS["5.1.01.00.001"] = ("Alquileres", True)
PLAN_DE_CUENTAS["5.1.01.00.002"] = ("Internet y Cable", True)
PLAN_DE_CUENTAS["5.1.01.00.003"] = ("Agua", True)
PLAN_DE_CUENTAS["5.1.01.00.004"] = ("Gas", True)
PLAN_DE_CUENTAS["5.1.01.00.005"] = ("Limpieza", True)
PLAN_DE_CUENTAS["5.1.02.00.000"] = ("Impuestos y Tasas", False)
PLAN_DE_CUENTAS["5.1.02.00.001"] = ("Impuesto Inmobiliario", True)
PLAN_DE_CUENTAS["5.1.02.00.002"] = ("Tasa Municipal", True)
PLAN_DE_CUENTAS["5.2.00.00.000"] = ("Gastos No Corrientes", False)


# CUENTAS
CODIGO_CUENTA_GASTOS = "5.1.00.00.000"
CODIGOS_CUENTAS_DISPONIBILIDADES = ["1.1.01.00.000", ]
CODIGO_CUENTA_APORTES_MENSUALES = "4.0.00.01.001"
CODIGO_CUENTA_CUOTAS_A_COBRAR = "1.1.02.01.000"
CODIGOS_CUENTAS_OTROS_ACTIVOS = ["1.2.01.00.000", "1.2.02.00.000"]


def CUENTA_GASTOS():
    return utils.get_cuenta_from_ejercicio_activo(CODIGO_CUENTA_GASTOS)


def CUENTA_APORTES_MENSUALES():
    return utils.get_cuenta_from_ejercicio_activo(
        CODIGO_CUENTA_APORTES_MENSUALES)


def CUENTA_CUOTAS_A_COBRAR():
    return utils.get_cuenta_from_ejercicio_activo(
        CODIGO_CUENTA_CUOTAS_A_COBRAR)
