# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0007_compromiso_observaciones'),
    ]

    operations = [
        migrations.AddField(
            model_name='compromiso',
            name='nombre',
            field=models.CharField(max_length=127, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='compromiso',
            name='user',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
