# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0002_auto_20160108_2223'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimiento',
            name='fecha',
            field=models.DateField(default=datetime.datetime(2016, 1, 9, 10, 30, 51, 923341, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='periodo',
            name='mes',
            field=models.PositiveIntegerField(choices=[(1, b'Enero'), (2, b'Febrero'), (3, b'Marzo'), (4, b'Abril'), (5, b'Mayo'), (6, b'Junio'), (7, b'Julio'), (8, b'Agosto'), (9, b'Septiembre'), (10, b'Octubre'), (11, b'Noviembre'), (12, b'Diciembre')]),
        ),
    ]
