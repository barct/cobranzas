# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0005_auto_20160113_1910'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='movimiento',
            name='periodo',
        ),
        migrations.RemoveField(
            model_name='movimiento',
            name='user',
        ),
        migrations.AddField(
            model_name='compromiso',
            name='codigo_cuenta',
            field=models.CharField(default='', max_length=13),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Movimiento',
        ),
    ]
