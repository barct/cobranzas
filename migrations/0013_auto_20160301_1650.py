# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0012_auto_20160219_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aportante',
            name='nivel',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='cobranzas.Nivel', null=True),
        ),
        migrations.AlterField(
            model_name='aportante',
            name='user',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
