# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0014_auto_20160305_0803'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='aportante',
            options={'permissions': (('can_view_aportante', 'Puede ver lista de aportantes'),)},
        ),
        migrations.AlterModelOptions(
            name='periodo',
            options={'permissions': (('can_view_periodo', 'Puede ver gastos e ingresos de periodos'),)},
        ),
    ]
