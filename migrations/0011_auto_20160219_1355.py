# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cobranzas', '0010_compromiso_email'),
    ]

    operations = [
        migrations.CreateModel(
            name='Aportante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombres', models.CharField(max_length=63, null=True, blank=True)),
                ('apellidos', models.CharField(max_length=63, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('codigo_cuenta', models.CharField(max_length=13)),
                ('monto', models.DecimalField(verbose_name=b'monto mensual', max_digits=18, decimal_places=2)),
                ('observaciones', models.TextField(null=True, blank=True)),
                ('nivel', models.ForeignKey(blank=True, to='cobranzas.Nivel', null=True)),
                ('user', models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='compromiso',
            name='nivel',
        ),
        migrations.RemoveField(
            model_name='compromiso',
            name='user',
        ),
        migrations.DeleteModel(
            name='Compromiso',
        ),
    ]
