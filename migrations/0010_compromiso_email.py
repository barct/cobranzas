# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0009_auto_20160219_1257'),
    ]

    operations = [
        migrations.AddField(
            model_name='compromiso',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
    ]
