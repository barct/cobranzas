# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0011_auto_20160219_1355'),
    ]

    operations = [
        migrations.RenameField(
            model_name='aportante',
            old_name='monto',
            new_name='compromiso',
        ),
    ]
