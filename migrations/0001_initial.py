# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cobro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('observaciones', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Compromiso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('monto', models.DecimalField(max_digits=18, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='Deuda',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('monto', models.DecimalField(max_digits=18, decimal_places=2)),
                ('concepto', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Nivel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nivel', models.CharField(max_length=31)),
            ],
        ),
        migrations.CreateModel(
            name='Periodo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mes', models.PositiveIntegerField()),
                ('ano', models.PositiveIntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='deuda',
            name='periodo',
            field=models.ForeignKey(blank=True, to='cobranzas.Periodo', null=True),
        ),
        migrations.AddField(
            model_name='deuda',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='compromiso',
            name='nivel',
            field=models.ForeignKey(blank=True, to='cobranzas.Nivel', null=True),
        ),
        migrations.AddField(
            model_name='compromiso',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='cobro',
            name='periodo',
            field=models.ForeignKey(to='cobranzas.Periodo'),
        ),
        migrations.AddField(
            model_name='cobro',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
