# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0017_auto_20160309_0803'),
    ]

    operations = [
        migrations.AddField(
            model_name='aportante',
            name='fecha_baja',
            field=models.DateField(null=True, blank=True),
        ),
    ]
