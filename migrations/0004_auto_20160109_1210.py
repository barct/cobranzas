# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0003_auto_20160109_0730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='compromiso',
            name='monto',
            field=models.DecimalField(verbose_name=b'monto mensual', max_digits=18, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='movimiento',
            name='fecha',
            field=models.DateField(),
        ),
    ]
