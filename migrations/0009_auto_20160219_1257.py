# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0008_auto_20160219_1253'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='compromiso',
            name='nombre',
        ),
        migrations.AddField(
            model_name='compromiso',
            name='apellidos',
            field=models.CharField(max_length=63, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='compromiso',
            name='nombres',
            field=models.CharField(max_length=63, null=True, blank=True),
        ),
    ]
