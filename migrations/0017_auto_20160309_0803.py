# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0016_auto_20160308_0632'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aportante',
            name='compartir_info',
            field=models.BooleanField(default=False),
        ),
    ]
