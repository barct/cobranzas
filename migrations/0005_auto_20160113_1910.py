# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0004_auto_20160109_1210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movimiento',
            name='fecha',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
