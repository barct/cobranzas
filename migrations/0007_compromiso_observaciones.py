# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0006_auto_20160121_1315'),
    ]

    operations = [
        migrations.AddField(
            model_name='compromiso',
            name='observaciones',
            field=models.TextField(null=True, blank=True),
        ),
    ]
