# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cobranzas', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Movimiento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('monto', models.DecimalField(max_digits=18, decimal_places=2)),
                ('concepto', models.TextField()),
                ('periodo', models.ForeignKey(blank=True, to='cobranzas.Periodo', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='cobro',
            name='periodo',
        ),
        migrations.RemoveField(
            model_name='cobro',
            name='user',
        ),
        migrations.RemoveField(
            model_name='deuda',
            name='periodo',
        ),
        migrations.RemoveField(
            model_name='deuda',
            name='user',
        ),
        migrations.DeleteModel(
            name='Cobro',
        ),
        migrations.DeleteModel(
            name='Deuda',
        ),
    ]
