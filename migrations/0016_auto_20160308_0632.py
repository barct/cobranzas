# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobranzas', '0015_auto_20160305_0815'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='aportante',
            options={'permissions': (('view_aportante', 'Puede ver lista de aportantes'),)},
        ),
        migrations.AlterModelOptions(
            name='periodo',
            options={'permissions': (('view_periodo', 'Puede ver gastos e ingresos de periodos'),)},
        ),
        migrations.AddField(
            model_name='aportante',
            name='compartir_info',
            field=models.BooleanField(default=True),
        ),
    ]
