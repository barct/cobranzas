# -*- coding: utf-8 *-*
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from contable.models import Cuenta, Ejercicio, RegistroAsiento
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from datetime import date
import calendar
import config
from django.utils.functional import cached_property


def DatosIniciales():
    e2016, c = Ejercicio.objects.get_or_create(
        id=1, fecha_inicio="2016-01-01", fecha_fin="2016-12-31", ano=2016)
    for codigo in config.PLAN_DE_CUENTAS:
        nombre = config.PLAN_DE_CUENTAS[codigo][0]
        imputable = config.PLAN_DE_CUENTAS[codigo][1]
        Cuenta.objects.get_or_create(
            codigo=codigo, ejercicio=e2016, nombre=nombre, imputable=imputable)


class Periodo(models.Model):
    class Meta:
        permissions = (
            ("view_periodo", "Puede ver gastos e ingresos de periodos"),
        )

    MES_ENERO = 1
    MES_FEBRERO = 2
    MES_MARZO = 3
    MES_ABRIL = 4
    MES_MAYO = 5
    MES_JUNIO = 6
    MES_JULIO = 7
    MES_AGOSTO = 8
    MES_SEPTIEMBRE = 9
    MES_OCTUBRE = 10
    MES_NOVIEMBRE = 11
    MES_DICIEMBRE = 12

    MES_CHOICES = (
        (MES_ENERO, "Enero"),
        (MES_FEBRERO, "Febrero"),
        (MES_MARZO, "Marzo"),
        (MES_ABRIL, "Abril"),
        (MES_MAYO, "Mayo"),
        (MES_JUNIO, "Junio"),
        (MES_JULIO, "Julio"),
        (MES_AGOSTO, "Agosto"),
        (MES_SEPTIEMBRE, "Septiembre"),
        (MES_OCTUBRE, "Octubre"),
        (MES_NOVIEMBRE, "Noviembre"),
        (MES_DICIEMBRE, "Diciembre"),
    )
    mes = models.PositiveIntegerField(choices=MES_CHOICES)
    ano = models.PositiveIntegerField()

    def __unicode__(self):
        return "%s/%s" % (self.mes, self.ano)

    def first_day(self):
        return date(year=self.ano, month=self.mes, day=1)

    def last_day(self):
        rango = calendar.monthrange(self.ano, self.mes)
        return date(year=self.ano, month=self.mes, day=rango[1])

    @cached_property
    def ejercicio(self):
        eje = Ejercicio.get_ejercicio_by_date(
            self.first_day())
        if eje:
            return eje
        else:
            raise Exception(
                "No hay ejercicio contable asignado a este periodo")

    def get_ejercicio(self):
        return self.ejercicio

    def save(self, *args, **kwargs):
        self.get_ejercicio()  # check if ejercicio is well configured
        s = super(Periodo, self).save(*args, **kwargs)
        return s

    def next(self):
        mes = self.mes
        ano = self.ano
        mes = mes + 1
        if mes > 12:
            ano += 1
            mes = 1
        periodo = Periodo.objects.filter(mes=mes, ano=ano)
        if periodo.count() > 0:
            return periodo[0]
        else:
            return None

    def get_movimientos_gastos(self):
        cuenta_gastos = self.get_ejercicio().cuenta_set.filter(
            codigo=config.CODIGO_CUENTA_GASTOS)
        if cuenta_gastos.count() == 1:
            cuenta_gastos = cuenta_gastos[0]
        else:
            raise Exception(
                "%s: Cuenta Gastos no se encuentra en el plan de cuentas" %
                config.CODIGO_CUENTA_GASTOS)
        gastos = cuenta_gastos.get_movimientos().filter(
            debe__gt=0,
            asiento__imputacion__gte=self.first_day(),
            asiento__imputacion__lte=self.last_day(),
            asiento__tipo=0
        ).order_by("asiento__imputacion")
        return gastos

    def get_movimientos_ingresos(self):
        cuenta_ingresos = self.get_ejercicio().cuenta_set.filter(
            codigo=config.CODIGO_CUENTA_CUOTAS_A_COBRAR)
        if cuenta_ingresos.count() == 1:
            cuenta_ingresos = cuenta_ingresos[0]
        else:
            raise Exception(
                "%s: Cuenta Ingresos no se encuentra en el plan de cuentas" %
                config.CODIGO_CUENTA_CUOTAS_A_COBRAR)

        ingresos = cuenta_ingresos.get_movimientos().filter(
            haber__gt=0,
            asiento__imputacion__gte=self.first_day(),
            asiento__imputacion__lte=self.last_day(),
            asiento__tipo=0
        ).order_by("asiento__imputacion")
        return ingresos

    def get_saldo_disponibilidades(self):
        eje = self.get_ejercicio()
        fecha_fin = self.last_day()
        cuentas = Cuenta.objects.filter(
            ejercicio=eje, codigo__in=config.CODIGOS_CUENTAS_DISPONIBILIDADES)
        ids = list()
        for c in cuentas:
            ids += c.get_rel_ids()
        cuentas = Cuenta.listadoCompleto(
            eje.id, ids=ids, fecha_fin=fecha_fin, tipo__in=(0, 1))
        return cuentas

    def get_otros_activos(self):
        eje = self.get_ejercicio()
        fecha_fin = self.last_day()
        cuentas = Cuenta.objects.filter(
            ejercicio=eje, codigo__in=config.CODIGOS_CUENTAS_OTROS_ACTIVOS)
        ids = list()
        for c in cuentas:
            ids += c.get_rel_ids()
        cuentas = Cuenta.listadoCompleto(
            eje.id, ids=ids, fecha_fin=fecha_fin, tipo__in=(0, 1))
        return cuentas


class Nivel(models.Model):
    nivel = models.CharField(max_length=31)

    def __unicode__(self):
        return self.nivel


@receiver(post_save, sender=User)
def update_user(sender, instance, created, **kwargs):
    if created:
        if instance.email:
            res = Aportante.objects.filter(email=instance.email)
            if res.count() == 1:
                ap = res[0]
                ap.user = instance
                ap.save()
    if (not hasattr(instance, 'aportante') and
       (instance.first_name or instance.last_name)):
        res = Aportante.objects.filter(
            nombres__icontains=instance.first_name,
            apellidos__icontains=instance.last_name,
            user__isnull=True)
        if res.count() == 1:
            ap = res[0]
            ap.user = instance
            ap.save()


class Aportante(models.Model):
    class Meta:
        permissions = (
            ("view_aportante", "Puede ver lista de aportantes"),
        )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True, blank=True)
    nombres = models.CharField(max_length=63, null=True, blank=True)
    apellidos = models.CharField(max_length=63, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    codigo_cuenta = models.CharField(max_length=13)
    compromiso = models.DecimalField(
        verbose_name='monto mensual', max_digits=18, decimal_places=2)
    nivel = models.ForeignKey(
        Nivel, on_delete=models.SET_NULL, null=True, blank=True)
    observaciones = models.TextField(null=True, blank=True)
    compartir_info = models.BooleanField(default=False)

    fecha_baja = models.DateField(null=True, blank=True)

    def saldo(self):
        cuenta = self.get_cuenta()
        if cuenta:
            return cuenta.saldo.resultado
        else:
            return 0

    def get_cuenta(self):
        if self.codigo_cuenta:
            try:
                cuenta = Cuenta.objects.get(
                    codigo=self.codigo_cuenta,
                    ejercicio=Ejercicio.get_activo())
            except ObjectDoesNotExist:
                return None
            return cuenta
        else:
            return None

    def save(self, *args, **kwargs):
        if self.codigo_cuenta is None or self.codigo_cuenta == "":
            self.codigo_cuenta = self.get_cuenta_padre().get_codigo_siguiente()
            Cuenta.objects.create(
                ejercicio=Ejercicio.get_activo(),
                codigo=self.codigo_cuenta,
                nombre=self.full_name() + " a cobrar",
                imputable=True)
        else:  # TODO: mejorar esto
            cuenta = Cuenta.objects.filter(
                codigo=self.codigo_cuenta, ejercicio=Ejercicio.get_activo())
            if cuenta.count() == 0:
                Cuenta.objects.create(
                    ejercicio=Ejercicio.get_activo(),
                    codigo=self.codigo_cuenta,
                    nombre=self.full_name() + " a cobrar",
                    imputable=True)
        return super(Aportante, self).save(*args, **kwargs)

    def full_name(self):
        if hasattr(self.user, "profile"):
            return self.user.profile.full_name
        else:
            return "%s %s" % (self.nombres, self.apellidos)

    @staticmethod
    def get_cuenta_padre():
        """retorna cual es la cuenta de activo cuotas a cobrar"""
        return config.CUENTA_CUOTAS_A_COBRAR()

    def __unicode__(self):
        return self.full_name()

    def get_movimientos(self):
        acumulado = 0
        cuentas = Cuenta.objects.filter(codigo=self.codigo_cuenta)
        ids = list()
        for c in cuentas:
            ids += c.get_rel_ids()
        ra = RegistroAsiento.objects.filter(cuenta_id__in=ids, asiento__tipo=0)
        movs = ra.order_by("asiento__imputacion", "asiento__numero")
        for m in movs:
            acumulado += m.saldo.resultado
            m.acumulado = acumulado
        return movs

    def get_saldo(self):
        cuenta = Cuenta.objects.get(
            codigo=self.codigo_cuenta, ejercicio=Ejercicio.get_activo())
        return cuenta.saldo
