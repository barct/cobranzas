# -*- encoding: utf-8 -*-
import models
import forms
from generic_views.views import ListView, register_manager
from django.http import HttpResponse
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, reverse
from django.contrib.auth.models import User
from django.core import serializers
from django.utils import timezone
from contable.models import Asiento, RegistroAsiento, Ejercicio, Cuenta
from django.db import transaction
from django.contrib.auth.decorators import permission_required
import config
import json
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.contrib.sites.models import Site
from django.views.generic.edit import FormView
# from generic_views.libs.debug import oprint
from django.utils.decorators import classonlymethod
from django.conf.urls import include, url
import utils
from generic_views.decorators import render_exhibible_errors, ExhibibleError


def no_ejercicio(request):
    template = loader.get_template("cobranzas/no_ejercicio.html")
    return HttpResponse(template.render({}, request))


@permission_required('cobranzas.view_aportante', raise_exception=True)
def panel(request):
    ejercicio = models.Ejercicio.get_activo()
    if not ejercicio or ejercicio.cuenta_set.count() == 0:
        return no_ejercicio(request)

    order = request.GET.get("order", "apellidos")
    if order == "apellidos":
        def order_func(a):
            return a.apellidos
    elif order == "nombres":
        def order_func(a):
            return a.nombres
    elif order == "deuda":
        def order_func(a):
            return a.saldo() * -1
    elif order == "aporte":
        def order_func(a):
            return a.compromiso * -1

    aportantes = models.Aportante.objects.filter(fecha_baja__isnull=True)
    total_compromisos = 0
    total_deuda = 0
    for a in aportantes:
        total_compromisos += a.compromiso
        total_deuda += a.saldo()

    aportantes = sorted(aportantes, key=order_func)

    movimientos = models.Aportante.get_cuenta_padre().get_movimientos().filter(
        debe__lte=0, asiento__tipo=0).order_by("-asiento__imputacion")[:20]

    gastos = config.CUENTA_GASTOS().get_movimientos().filter(
        debe__gte=0, asiento__tipo=0).order_by("-asiento__imputacion")[:20]
    cd = Cuenta.objects.filter(
        ejercicio=ejercicio,
        codigo__in=config.CODIGOS_CUENTAS_DISPONIBILIDADES)
    saldos_ids = list()
    for c in cd:
        saldos_ids += c.get_rel_ids()

    saldos = models.Cuenta.listadoCompleto(
        ejercicio_id=ejercicio.pk,
        hide_inuse=False,
        ids=saldos_ids,
        tipo__in=(0, 1))

    template = loader.get_template("cobranzas/panel.html")
    context = {
        "aportantes": aportantes,
        "movimientos": movimientos,
        "gastos": gastos,
        "saldos": saldos,
        "total_compromisos": total_compromisos,
        "total_deuda": total_deuda,
        "ejercicio": ejercicio,
    }
    return HttpResponse(template.render(context, request))


register_manager.register_menu("finanzas", {
                               'label': "Cobros",
                               'url': "/cobranzas/panel/",
                               "permissions": "cobranzas.view_aportante"})


@permission_required('cobranzas.view_periodo', raise_exception=True)
def periodos(request):
    periodos = models.Periodo.objects.all().order_by("-ano", "-mes")[:13]
    template = loader.get_template("cobranzas/periodos.html")
    context = {
        "periodos": periodos,
    }
    return HttpResponse(template.render(context, request))


register_manager.register_menu("finanzas", {
                               'label': "Ingresos y Gastos",
                               'url': "/cobranzas/periodos/",
                               "permissions": "cobranzas.view_periodo"})


@permission_required('cobranzas.change_aportante', raise_exception=True)
@transaction.atomic
def cobro_cuota(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = forms.CobroCuota(request.POST)
        # check whether it's valid:
        if form.is_valid():
            aportante = form.cleaned_data["aportante"]
            periodos = form.cleaned_data["periodos"]
            imputacion = form.cleaned_data["imputacion"]
            monto = form.cleaned_data["monto"]
            descripcion = form.cleaned_data["descripcion"]
            cuenta = form.cleaned_data["cuenta"]

            periodos_verbose = "Cobro de cuota aporte periodos: " + \
                ", ".join([str(p) for p in periodos])

            eje = Ejercicio.get_activo()
            cuenta_usuario = Cuenta.objects.get(
                ejercicio=eje, codigo=aportante.codigo_cuenta)
            asiento = Asiento.objects.create(ejercicio=eje,
                                             imputacion=imputacion,
                                             carga=timezone.now(),
                                             descripcion=periodos_verbose +
                                             descripcion)
            RegistroAsiento.objects.create(
                asiento=asiento, cuenta=cuenta, debe=monto, haber=0)
            RegistroAsiento.objects.create(
                asiento=asiento, cuenta=cuenta_usuario, debe=0, haber=monto)

            if aportante.user is not None:
                send_cobro_mail(aportante, monto, descripcion)

            content = serializers.serialize("json", (asiento,))
            response = HttpResponse(
                content_type='application/json; charset=utf-8',
                content=content)
            return response
    else:
        init = request.GET.copy()
        init["imputacion"] = timezone.now()
        form = forms.CobroCuota(initial=init)
    return render(request, 'generic_views/form_view.html', {'form': form})


@permission_required('cobranzas.change_aportante', raise_exception=True)
@transaction.atomic
def registro_gasto(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = forms.RegistroGasto(request.POST)
        # check whether it's valid:
        if form.is_valid():
            periodo = form.cleaned_data["periodo"]
            monto = form.cleaned_data["monto"]
            imputacion = form.cleaned_data["imputacion"]
            descripcion = form.cleaned_data["descripcion"]
            cuenta = form.cleaned_data["cuenta"]
            gasto = form.cleaned_data["gasto"]

            eje = Ejercicio.get_activo()

            asiento = Asiento.objects.create(
                ejercicio=eje,
                imputacion=imputacion,
                carga=timezone.now(),
                descripcion=str(periodo) + "\n" + descripcion)
            RegistroAsiento.objects.create(
                asiento=asiento, cuenta=gasto, debe=monto, haber=0)
            RegistroAsiento.objects.create(
                asiento=asiento, cuenta=cuenta, debe=0, haber=monto)

            content = serializers.serialize("json", (asiento,))
            response = HttpResponse(
                content_type='application/json; charset=utf-8',
                content=content)
            return response
    else:
        init = request.GET.copy()
        init["imputacion"] = timezone.now()
        form = forms.RegistroGasto(initial=init)
    return render(request, 'generic_views/form_view.html', {'form': form})


class AportanteCreateView(FormView):
    template_name = 'generic_views/form_view.html'
    form_class = forms.AportanteCreateForm
    view_name = "create"
    list_url_template = "generic_views/urls/create.html"
    pk_var = 'pk'
    popup = True

    @transaction.atomic
    def form_valid(self, form):
        if form.is_valid():
            nombres = form.cleaned_data["nombres"]
            apellidos = form.cleaned_data["apellidos"]
            email = form.cleaned_data["email"]
            nivel = form.cleaned_data["nivel"]
            compromiso = form.cleaned_data["compromiso"]
            periodo = form.cleaned_data["periodo"]
            usuario = form.cleaned_data["usuario"]

            if email:
                users = User.objects.filter(email=email)
                if users.count() > 0:
                    user = users[0]
                    nombres = user.first_name
                    apellidos = user.last_name
                    if hasattr(user, "aportante"):
                        raise "El aportante ya existe"
                else:
                    user = User.objects.create_user(usuario, email, "pinky")
                    user.first_name = nombres
                    user.last_name = apellidos
                    user.save()

                # Profile.objects.create(user=user)
            else:
                user = None

            aportante_obj = models.Aportante.objects.create(
                nombres=nombres,
                apellidos=apellidos,
                email=email,
                user=user,
                nivel=nivel,
                compromiso=compromiso)

            if periodo:
                p = periodo
                monto = 0
                periodos_verbose = "Devengado de aportes de periodos: "
                while p:
                    monto += compromiso
                    periodos_verbose += str(p) + ", "
                    p = p.next()

                eje = Ejercicio.get_activo()
                asiento = Asiento.objects.create(
                    ejercicio=eje,
                    imputacion=timezone.now(),
                    carga=timezone.now(),
                    descripcion=periodos_verbose)
                aportes_mensuales = config.CUENTA_APORTES_MENSUALES()
                cuenta_usuario = Cuenta.objects.get(
                    ejercicio=eje, codigo=aportante_obj.codigo_cuenta)
                RegistroAsiento.objects.create(
                    asiento=asiento,
                    cuenta=aportes_mensuales,
                    haber=monto,
                    debe=0)
                RegistroAsiento.objects.create(
                    asiento=asiento,
                    cuenta=cuenta_usuario,
                    haber=0,
                    debe=monto)

            content = serializers.serialize("json", (aportante_obj,))
            response = HttpResponse(
                content_type='application/json; charset=utf-8',
                content=content)

            if email:
                utils.send_reset_form(self.request, email)

            return response

    @classonlymethod
    def as_view(cls, **initkwargs):
        urls = [
            url(r'^add/$', super(AportanteCreateView, cls).as_view(
                **initkwargs), name="create"),
        ]
        return include(urls)


class AportanteUpdateView(FormView):
    template_name = 'generic_views/form_view.html'
    form_class = forms.AportanteUpdateForm
    view_name = "update"
    list_url_template = "generic_views/urls/update.html"
    pk_var = 'pk'
    popup = True

    def get_initial(self):
        initial = super(AportanteUpdateView, self).get_initial()
        if (hasattr(self, 'aportante')):
            aportante = self.aportante
            initial["aportante_id"] = aportante.pk
            initial["nombres"] = aportante.nombres
            initial["apellidos"] = aportante.apellidos
            initial["compromiso"] = aportante.compromiso
            initial["nivel"] = aportante.nivel
            if aportante.user:
                initial["email"] = aportante.user.email
            initial["fecha_baja"] = aportante.fecha_baja
            return initial

    def get(self, request, *args, **kwargs):
        self.aportante = models.Aportante.objects.get(pk=kwargs["pk"])
        return super(AportanteUpdateView, self).get(request, *args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        if form.is_valid():
            nombres = form.cleaned_data["nombres"]
            apellidos = form.cleaned_data["apellidos"]
            email = form.cleaned_data["email"]
            nivel = form.cleaned_data["nivel"]
            compromiso = form.cleaned_data["compromiso"]
            aportante = form.cleaned_data["aportante"]
            fecha_baja = form.cleaned_data["fecha_baja"]
            user = aportante.user
            if not user and email:
                username = utils.make_username(nombres, apellidos)
                users = User.objects.filter(username=username)
                if users.count() > 0:
                    aportante.user = users[0]
                else:
                    aportante.user = User.objects.create_user(
                        username, email, "pinky")
                utils.send_reset_form(self.request, email)
            if aportante.user:
                aportante.user.first_name = nombres
                aportante.user.last_name = apellidos
                aportante.user.email = email
                aportante.user.save()
            aportante.compromiso = compromiso
            aportante.nivel = nivel
            aportante.fecha_baja = fecha_baja
            aportante.nombres = nombres
            aportante.apellidos = apellidos
            aportante.save()

            content = serializers.serialize("json", (aportante,))
            response = HttpResponse(
                content_type='application/json; charset=utf-8',
                content=content)
            return response

    @classonlymethod
    def as_view(cls, **initkwargs):
        urls = [
            url(r'^update/(?P<%s>[^\\]+)/$' % cls.pk_var,
                super(AportanteUpdateView, cls
                      ).as_view(**initkwargs), name="update"),
        ]

        return include(urls)


# class AportanteCrud(MixinView):
#     model = models.Aportante
#     fields = ['nivel', 'compromiso', 'fecha_baja']

#     permissions_create = 'cobranzas.add_aportante'
#     permissions_change = 'cobranzas.change_aportante'
#     permissions_delete = 'cobranzas.delete_aportante'


class AportanteList(ListView):
    module_name = "finanzas"
    objects_list = models.Aportante.objects.all()
    list_display = ['nombres', 'apellidos',
                    'nivel', 'compromiso', 'user__email', 'fecha_baja']
    filter_fields = ['nivel', 'compromiso']

    create_view = AportanteCreateView
    update_view = AportanteUpdateView

    permissions = 'cobranzas.view_aportante'

    @staticmethod
    def get_menu():
        return {
            'label': "Aportantes", 'url': reverse("cobranzas:aportantes:list"),
            "permissions": "cobranzas.view_aportante"}


def send_deuda_mail(aportante):
    current_site = Site.objects.get_current()
    subject = 'Resúmen de aportes voluntarios'
    to = (aportante.user.email, )
    message = get_template(
        'cobranzas/email_deuda.html').render({'aportante': aportante,
                                              'domain': current_site.domain,
                                              'protocol': 'http'})
    msg = EmailMessage(subject, message, to=to)
    msg.content_subtype = 'html'
    msg.send()


def send_cobro_mail(aportante, monto, descripcion):
    current_site = Site.objects.get_current()
    subject = 'Se ha registrado tu aporte voluntario'
    to = (aportante.user.email, )
    message = get_template(
        'cobranzas/email_cobro.html').render(
        {'aportante': aportante,
         'monto': monto,
         'descripcion': descripcion,
         'protocol': 'http',
         'domain': current_site.domain,
         })
    msg = EmailMessage(subject, message, to=to)
    msg.content_subtype = 'html'
    msg.send()


def test(request):
    aportante = request.user.aportante
    template = loader.get_template("cobranzas/email_deuda.html")
    context = {
        "aportante": aportante,
        "perms": None,
    }
    return HttpResponse(template.render(context, request))


def devengar_aportes(periodo):
    eje = periodo.get_ejercicio()
    descripcion = "Devengado de Cuotas de Aportes %s" % periodo
    asiento = Asiento.objects.create(
        ejercicio=eje,
        imputacion=periodo.first_day(),
        descripcion=descripcion)
    aportantes = models.Aportante.objects.filter(
        fecha_baja__isnull=True)

    cuenta_aportes_mensuales = eje.cuenta_set.filter(
        codigo=config.CODIGO_CUENTA_APORTES_MENSUALES)
    if cuenta_aportes_mensuales.count() == 1:
        cuenta_aportes_mensuales = cuenta_aportes_mensuales[0]
    else:
        raise ExhibibleError(
            None, "POPUP", "Error de Configuración",
            "La Cuenta <strong>%s</strong> correspondiente a las "
            "Cuentas de Aportes no se encuentra en "
            "en el plan de cuentas del ejercicio %s" %
            (config.CODIGO_CUENTA_APORTES_MENSUALES, eje))

    total = 0
    for ap in aportantes:
        if ap.compromiso > 0:
            total += ap.compromiso

            cuenta_usuario = Cuenta.objects.get(
                ejercicio=eje, codigo=ap.codigo_cuenta)
            # TODO: si no la encuentra tiene que crearla

            RegistroAsiento.objects.create(
                asiento=asiento,
                cuenta=cuenta_usuario,
                haber=0,
                debe=ap.compromiso)
        if ap.user and ap.user.email:
            send_deuda_mail(ap)

    RegistroAsiento.objects.create(
        asiento=asiento,
        cuenta=cuenta_aportes_mensuales,
        haber=total,
        debe=0)


@render_exhibible_errors
@permission_required('cobranzas.add_periodo', raise_exception=True)
@transaction.atomic
def abrir_periodo(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = forms.AbrirPeriodo(request.POST)
        # check whether it's valid:
        if form.is_valid():
            try:
                periodo = form.save()
            except Exception:
                raise ExhibibleError(
                    request, "POPUP", "Error!", "Ejercicio mal configurado")

            if form.cleaned_data["devengar"] is True:
                devengar_aportes(periodo)
            content = serializers.serialize("json", (periodo,))
            response = HttpResponse(
                content_type='application/json; charset=utf-8',
                content=content)
            return response
    else:
        form = forms.AbrirPeriodo()
    return render(request, 'generic_views/form_view.html', {'form': form})


def aportante_own(request):
    try:
        aportante = models.Aportante.objects.get(pk=request.user.aportante.id)
    except ObjectDoesNotExist:
        aportante = None
    template = loader.get_template("cobranzas/aportante_show.html")
    context = {
        "aportante": aportante,
    }
    return HttpResponse(template.render(context, request))


register_manager.register_menu(
    "finanzas",
    {'label': "Mis Aportes", 'url': '/cobranzas/aportantes/aportante/own/', })


@permission_required('cobranzas.view_aportante', raise_exception=True)
def aportante_show(request, aportante_id):

    aportante = models.Aportante.objects.get(pk=aportante_id)

    template = loader.get_template("cobranzas/aportante_show.html")
    context = {
        "aportante": aportante,
    }
    return HttpResponse(template.render(context, request))


def change_profile(request):
    content = {"status": "FAILURE"}
    if request.method == "POST":
        value = request.POST.get("value", None)
        if value is not None:
            value = value.strip()
        else:
            value = None
        name = request.POST.get("name", None)

        user = request.user

        if name == "compartir_info" and value is not None:
            user.aportante.compartir_info = True if value == "1" else False
        else:
            raise ObjectDoesNotExist
        user.aportante.save()
        content["status"] = "OK"

    response = HttpResponse(
        content_type='application/json; charset=utf-8',
        content=json.dumps(content))
    return response
