from django.contrib import admin

# Register your models here.
import models


registros_basicos = (

    models.Periodo,
    models.Nivel,
    models.Aportante,

)

for registro in registros_basicos:
    admin.site.register(registro)
