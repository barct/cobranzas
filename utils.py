import models
from django.contrib.auth.forms import PasswordResetForm


def get_cuenta_from_ejercicio_activo(codigo_cuenta):
    return models.Cuenta.objects.get(
        codigo=codigo_cuenta, ejercicio=models.Ejercicio.get_activo())


def send_reset_form(request, email):
    reset_form = PasswordResetForm({'email': email})
    assert reset_form.is_valid()
    reset_form.save(
        request=request,
        # use_https=request.is_secure(),
        subject_template_name='cobranzas/'
                              'new_aportante_subject.txt',
        html_email_template_name='cobranzas/new_aportante.html',
        # from_email="webmaster@tribunaperonista.com.ar",
    )


def make_username(nombres, apellidos):
    n = nombres.split(' ')[0][0]
    a = apellidos.split(' ')[0]
    username = n.lower() + "." + a.lower()
    return username
