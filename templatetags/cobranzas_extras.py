# -*- coding: utf-8 -*-
from django.template import Library
from decimal import Decimal
from contable.utils import PartidaDoble

register = Library()


@register.filter_function
def invert(value, efective=True):
    if efective:
        if isinstance(value, Decimal):
            if value != 0.0:
                return value * -1
            else:
                return 0.0
        if isinstance(value, PartidaDoble):
            return PartidaDoble(value.haber, value.debe)
    else:
        return value
