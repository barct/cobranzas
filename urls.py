from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from generic_views.urls import decorated_includes
import views

app_name = "cobranzas"

urlpatterns = [

    url(r'^aportantes/',
        decorated_includes(
            login_required, views.AportanteList.as_view(namespace="aportantes"))),
    url(r'^panel/$', login_required(views.panel), name="panel"),
    url(r'^test/$', login_required(views.test), name="test"),
    url(r'^periodos/abrir/$',
        login_required(views.abrir_periodo),
        name="abrir-periodo"),
    url(r'^periodos/$', login_required(views.periodos), name="periodos"),
    url(r'^aportantes/cobro/add/$',
        login_required(views.cobro_cuota), name="cobro-add"),
    url(r'^aportantes/gasto/add/$',
        login_required(views.registro_gasto), name="gasto-add"),
    url(r'^aportantes/aportante/show/(?P<aportante_id>\d+)/$',
        login_required(views.aportante_show), name="aportante-show"),
    url(r'^aportantes/aportante/change_profile/$',
        login_required(views.change_profile), name="aportante-change-profile"),
    url(r'^aportantes/aportante/own/$',
        login_required(views.aportante_own), name="aportante-own"),

]
