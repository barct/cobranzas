# -*- encoding: utf-8 -*-
from django import forms
import models
from django.contrib.auth.models import User
from contable.models import Cuenta, Ejercicio
from generic_views import widgets
import config
from generic_views.libs.debug import oprint
from django.db.models import Q
import datetime
import utils


class AportanteCreateForm(forms.Form):
    nombres = forms.CharField(max_length=63)
    apellidos = forms.CharField(max_length=63)
    email = forms.EmailField(required=False)
    nivel = forms.ModelChoiceField(
        queryset=models.Nivel.objects.all(), required=False)
    compromiso = forms.DecimalField(max_digits=18, decimal_places=2)
    periodo = forms.ModelChoiceField(
        label='Periodo desde que aporta',
        queryset=models.Periodo.objects.all().order_by(
            "-ano", "-mes"), required=False)

    def clean(self):
        cleaned_data = super(AportanteCreateForm, self).clean()
        nombres = cleaned_data.get("nombres")
        apellidos = cleaned_data.get("apellidos")
        email = cleaned_data.get("email")

        if email:
            users = User.objects.filter(email=email)
            if users.count() > 0:
                user = users[0]
                if hasattr(user, "aportante"):
                    raise forms.ValidationError(
                        {"email": ("El aportante ya existe")})
        if nombres and apellidos:
            username = utils.make_username(nombres, apellidos)
            users = User.objects.filter(username=username)
            if users.count() > 0:
                user = users[0]
                if hasattr(user, "aportante"):
                    raise forms.ValidationError(
                        {"nombres": ("El aportante ya existe")})
                else:
                    cleaned_data["usuario"] = user.username
            else:
                cleaned_data["usuario"] = username
        return cleaned_data


class AportanteUpdateForm(forms.Form):
    aportante_id = forms.IntegerField(
        required=True, widget=forms.HiddenInput())
    nombres = forms.CharField(max_length=63)
    apellidos = forms.CharField(max_length=63)
    email = forms.EmailField(required=False)
    nivel = forms.ModelChoiceField(
        queryset=models.Nivel.objects.all(), required=False)
    compromiso = forms.DecimalField(max_digits=18, decimal_places=2)
    fecha_baja = forms.DateField(required=False)

    def clean(self):
        cleaned_data = super(AportanteUpdateForm, self).clean()
        aportante_id = cleaned_data.get("aportante_id")
        email = cleaned_data.get("email")
        cleaned_data["aportante"] = models.Aportante.objects.get(
            pk=aportante_id)

        if email:
            users = models.Aportante.objects.filter(
                ~Q(pk=aportante_id), user__email=email)
            if users.count() > 0:
                raise forms.ValidationError(
                    {"email": ("El email pertenece a otro aportante")})
        return cleaned_data


class CobroCuota(forms.Form):
    aportante = forms.ModelChoiceField(
        label='Aportante',
        queryset=models.Aportante.objects.all().order_by(
            "nombres", "apellidos"))
    periodos = forms.ModelMultipleChoiceField(
        label='Periodos',
        queryset=models.Periodo.objects.all().order_by("-ano", "-mes"),
        required=False)
    imputacion = forms.DateField(
        label="F. Imputación", widget=widgets.DatePicker)
    monto = forms.DecimalField(max_digits=18, decimal_places=2)
    cuenta = forms.ModelChoiceField(queryset=Cuenta.objects.all())
    descripcion = forms.CharField(widget=forms.Textarea, required=False)

    def __init__(self, *args, **kwargs):
        super(CobroCuota, self).__init__(*args, **kwargs)
        ids = Ejercicio.get_activo().get_rel_cuentas_ids_from_codigos(
            config.CODIGOS_CUENTAS_DISPONIBILIDADES)
        queryset = Cuenta.objects.filter(pk__in=ids)
        self.fields["cuenta"].widget = widgets.TreeView(
            queryset,
            parent_field="parent_id",
            selecteable_field="imputable")


class RegistroGasto(forms.Form):
    periodo = forms.ModelChoiceField(
        label='Período',
        queryset=models.Periodo.objects.all().order_by(
            "-ano", "-mes"),
        required=False)
    imputacion = forms.DateField(
        label="F. Imputación", widget=widgets.DatePicker)
    gasto = forms.ModelChoiceField(
        queryset=Cuenta.objects.all()
    )
    cuenta = forms.ModelChoiceField(
        queryset=Cuenta.objects.all()
    )
    monto = forms.DecimalField(max_digits=18, decimal_places=2)
    descripcion = forms.CharField(widget=forms.Textarea, required=False)

    def __init__(self, *args, **kwargs):
        super(RegistroGasto, self).__init__(*args, **kwargs)

        ids = Ejercicio.get_activo().get_rel_cuentas_ids_from_codigos(
            config.CODIGOS_CUENTAS_DISPONIBILIDADES)
        queryset = Cuenta.objects.filter(pk__in=ids)

        self.fields["cuenta"].widget = widgets.TreeView(
            queryset,
            parent_field="parent_id",
            selecteable_field="imputable")

        self.fields["gasto"].widget = widgets.TreeView(
            config.CUENTA_GASTOS().get_rel_cuentas(),
            parent_field="parent_id",
            selecteable_field="imputable")


class AbrirPeriodo(forms.ModelForm):
    class Meta:
        model = models.Periodo
        fields = ["mes", "ano"]

    devengar = forms.BooleanField(
        label="Devengar Cuotas", required=False, initial=True)

    def clean(self, *args, **kwargs):
        cleaned_data = super(AbrirPeriodo, self).clean()
        mes = cleaned_data.get("mes")
        ano = cleaned_data.get("ano")

        if mes and ano:
            eje = Ejercicio.get_ejercicio_by_date(datetime.date(ano, mes, 1))
            if not eje:
                raise forms.ValidationError(
                    {"mes": "No hay ejercicio contable para este periodo"})

        if models.Periodo.objects.filter(mes=mes, ano=ano).count() > 0:
            raise forms.ValidationError({"mes": "El periodo ya existe"})

        return cleaned_data
